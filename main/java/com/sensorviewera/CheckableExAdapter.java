package com.sensorviewera;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
public class CheckableExAdapter extends BaseExpandableListAdapter
{
    private LayoutInflater mInflater;
    private ArrayList<GroupItem> groups; 
    private OnCheckedListener cb_listener; 
    public CheckableExAdapter(Context context,ArrayList<GroupItem> groups)
    {
        this.mInflater=LayoutInflater.from(context);
        cb_listener=new OnCheckedListener();
        this.groups=groups;
    }

    public int count(){return groups.size();}
    public void setAllCBoxVisibility(boolean v)
    {
        Integer visibility=CheckBox.VISIBLE;
        if(!v)
            visibility=CheckBox.INVISIBLE;
        int g_size=groups.size();
        for (int i = 0; i <g_size ; i++)
            groups.get(i).isCheckboxVisible=visibility;
    }
    public void setAllChecked(boolean c)
    {
        int g_size=groups.size();
        for(int i=0;i<g_size;i++)
            groups.get(i).isChecked=c;
    }
    public void addGroup(int idx,GroupItem item)
    {
        item.isChecked=false;
        groups.add(idx,item);
    }
    public void removeItem(int idx)
    {
        groups.remove(idx);
    }
    public void removeGroup(GroupItem g)
    {
        groups.remove(g);
    }
    public GroupItem getGroupAt(int idx){return idx<count()&&idx>=0?groups.get(idx):null;}


    @Override
    public int getGroupCount()
    {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int i)
    {
        return groups.get(i).childrenCount();
    }

    @Override
    public Object getGroup(int i)
    {
        return groups.get(i);
    }

    @Override
    public Object getChild(int i, int i2)
    {
        return groups.get(i).getChild(i2);
    }

    @Override
    public long getGroupId(int i)
    {
        return 0;
    }

    @Override
    public long getChildId(int i, int i2)
    {
        return 0;
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    @Override
    public View getGroupView(final int position, boolean b, View view, ViewGroup viewGroup)
    {
        GroupViewHolder holder=null;
        if(view==null)
        {
            holder=new GroupViewHolder();
            view=mInflater.inflate(R.layout.group_item, null);
            holder.name=(TextView)view.findViewById(R.id.text_sensor_name);
            holder.model=(TextView)view.findViewById(R.id.text_sensor_model);
            holder.image_sending=(ImageView)view.findViewById(R.id.image_sending);
            holder.checked=(CheckBox)view.findViewById(R.id.check_is_send);
            holder.checked.setOnCheckedChangeListener(cb_listener);
            view.setTag(holder);
        }
        else
            holder = (GroupViewHolder) view.getTag();
        holder.name.setText(groups.get(position).majorText);
        holder.model.setText(groups.get(position).minorText);
        holder.checked.setTag(position);
        holder.checked.setChecked(groups.get(position).isChecked);
        holder.checked.setVisibility(groups.get(position).isCheckboxVisible);
        holder.image_sending.setImageBitmap(groups.get(position).image);
        view.setId(position);
        return view;
    }
    class OnCheckedListener implements CompoundButton.OnCheckedChangeListener
    {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
        {
            Integer p=(Integer)buttonView.getTag();
            if(isChecked)
                groups.get(p).isChecked=true;
            else
                groups.get(p).isChecked=false;
        }
    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup)
    {
        ChildViewHolder holder=null;
        if(view==null)
        {
            holder=new ChildViewHolder();
            view=mInflater.inflate(R.layout.child_item, null);
            holder.name=(TextView)view.findViewById(R.id.text_child_title);
            view.setTag(holder);
        }
        else
            holder = (ChildViewHolder) view.getTag();
        holder.name.setText(groups.get(i).getChild(i2).text);
        holder.item=groups.get(i).getChild(i2);
        view.setId(-1);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
final class GroupItem
{
    public SensorList parent;
    public String majorText;
    public String minorText;
    public String sensorType; 
    public Bitmap image=null;
    private ArrayList<ChildItem> children; //groupview的childview
    public boolean isChecked=false;
    public int isCheckboxVisible =CheckBox.INVISIBLE;
    public GroupItem(String ma_t,String mi_t,String s_type,ArrayList<ChildItem> cs)
    {majorText =ma_t;minorText=mi_t;sensorType =s_type;children=cs;}
    public ChildItem getChild(int idx){return children.get(idx);}
    public int childrenCount(){return children.size();}
    public void addChild(ChildItem c){children.add(c);}
    public void setChildren(ArrayList<ChildItem> children)
    {
        this.children=children;
    }
    public void setCBoxVisibility(boolean v)
    {
        int visibility=CheckBox.INVISIBLE;
        if(v)
            visibility=CheckBox.VISIBLE;
        isCheckboxVisible=visibility;
    }
}

final class ChildItem
{
    public String text;
    public boolean hasContextMenu=false;
    public int id;
    public ChildItem(String t,int id_)
    {text=t;hasContextMenu=false;id=id_;}
    public ChildItem(String t,int id_,boolean has_context_menu)
    {text=t;hasContextMenu=has_context_menu;id=id_;}
}

final class GroupViewHolder
{
    public TextView name;
    public TextView model;
    public CheckBox checked;
    public ImageView image_sending;
    public GroupItem item;
}

final class ChildViewHolder
{
    public TextView name;
    public ChildItem item;
}
