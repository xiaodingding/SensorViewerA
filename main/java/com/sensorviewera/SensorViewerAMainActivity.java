package com.sensorviewera;
//    SENSOR_DELAY_NORMAL (200000微秒)
//    SENSOR_DELAY_UI (60000微秒)
//    SENSOR_DELAY_GAME (20000微秒)
//    SENSOR_DELAY_FASTEST (0微秒)
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SensorViewerAMainActivity extends Activity
{
    public static final int CONTEXT_MENU_5Hz = Menu.FIRST+1;
    public static final int CONTEXT_MENU_10Hz = Menu.FIRST+2;
    public static final int CONTEXT_MENU_25Hz = Menu.FIRST+3;
    public static final int CONTEXT_MENU_50Hz = Menu.FIRST+4;

    private static final int CHILD_ITEM_TO_START_SENDING=1;
    private static final int CHILD_ITEM_DETAIL_INFO=2;
    private static final int CHILD_ITEM_START_SENDING=3;
    private static final int CHILD_ITEM_STOP_SENDING=4;
    private static final int CHILD_ITEM_REMOVE=5;
    private static final int CHILD_ITEM_FEQ=6;

    private static final String IP_FILE_NAME="IPAndPort"; 
    private static final int STORED_IPS=3;

    private SensorManager sm;
    HashMap<String,SensorOperation> sensors;
    SensorList sensor_list; 
    SendingSensorList sending_list;
    SensorList current_list;
    SensorList another_list;

    private ArrayList<GroupItem> sensor_list_groups;
    private ArrayList<ChildItem> sensor_list_children;
    private ArrayList<ChildItem> sending_list_children; 
    private ArrayList<ChildItem> current_children;
    private ArrayList<ChildItem> another_children;
    private  Menu option_menu=null;
    private GroupItem item_of_context_menu=null; 
    private Handler msg_handler =null; 
    private long exit_time=0;
    private boolean is_send_after_pause =false; 
    private boolean has_input_ip=false; 
    private boolean connected_server=false; 
    private ArrayList<String> last_ip; 

    private TextView tx;
    private AutoCompleteTextView et;

    private DataTrans data_trans =null; 



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        msg_handler =new MsgHandler();
        data_trans=new DataTrans(msg_handler);
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        SensorOperation.init(sm, data_trans.getSender());
        getLastIPAndPort(); 
        setupUI();
    }
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        SensorOperation op=sensors.get(item_of_context_menu.sensorType);
        switch(item.getItemId())
        {
            case CONTEXT_MENU_5Hz:
                op.setSamplingFre(5);
                break;
            case CONTEXT_MENU_10Hz:
                op.setSamplingFre(10);
                break;
            case CONTEXT_MENU_25Hz:
                op.setSamplingFre(25);
                break;
            case CONTEXT_MENU_50Hz:
                op.setSamplingFre(50);
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }
    private void setupUI()
    {
        setContentView(R.layout.activity_sensorviewera_main);
        sensor_list_children=new ArrayList<ChildItem>();
        sensor_list_children.add(new ChildItem(getString(R.string.detail_info),CHILD_ITEM_DETAIL_INFO));
        sending_list_children=new ArrayList<ChildItem>();
        sending_list_children.add(new ChildItem(getString(R.string.sending_start),CHILD_ITEM_START_SENDING));
        sending_list_children.add(new ChildItem(getString(R.string.sending_stop),CHILD_ITEM_STOP_SENDING));
        sending_list_children.add(new ChildItem(getString(R.string.sending_feq),CHILD_ITEM_FEQ));
        sending_list_children.add(new ChildItem(getString(R.string.sending_remove),CHILD_ITEM_REMOVE));
        sensors=getAvailableSensor();
        ExpandableListView  list_sensor=(ExpandableListView)findViewById(R.id.list_sensor);
        ExpandableListView  list_sending=(ExpandableListView)findViewById(R.id.list_sending);
        sensor_list=new SensorList(list_sensor,sensor_list_groups);
        sending_list=new SendingSensorList(list_sending);
        current_list=sensor_list; 
        another_list=sending_list;
        current_children=sensor_list_children;
        another_children=sending_list_children;
        int s_g=sensor_list_groups.size();
        for(int i=0;i<s_g;i++)
            sensor_list_groups.get(i).parent=sensor_list;

        TabHost tab_list = (TabHost) findViewById(R.id.tab_list);
        tab_list.setup();
        tab_list.addTab(tab_list.newTabSpec("available").setIndicator(getString(R.string.available_sensor)).setContent(R.id.list_sensor));
        tab_list.addTab(tab_list.newTabSpec("sending").setIndicator(getString(R.string.sending_sensor)).setContent(R.id.list_sending));
        tab_list.setOnTabChangedListener(new TabHost.OnTabChangeListener()
        {
            @Override
            public void onTabChanged(String s)
            {
                if(s.equals("available"))
                {
                    current_list=sensor_list;
                    another_list=sending_list;
                    current_children=sensor_list_children;
                    another_children=sending_list_children;
                    option_menu.getItem(0).setTitle(getApplicationContext().getString(R.string.to_sending_start));
                }
                else if(s.equals("sending"))
                {
                    current_list=sending_list;
                    another_list=sensor_list;
                    current_children=sending_list_children;
                    another_children=sensor_list_children;
                    option_menu.getItem(0).setTitle(getApplicationContext().getString(R.string.sending_remove));
                }
            }
        });
        tx=(TextView)findViewById(R.id.text_state);
        et=(AutoCompleteTextView)findViewById(R.id.autoedit_ip);
        et.setOnFocusChangeListener(new HasFocus());
        et.setOnKeyListener(new IPInputListener());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        option_menu=menu;
        return true;
    }
    @Override
    protected void onPause()
    {
        if(!is_send_after_pause)
            stopAllSending();
        super.onPause();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN)
        {
            if(current_list.isCheckboxVisible)
            {
                current_list.setAllChecked(false);
                exit_time=0;
                return true;
            }
            if((System.currentTimeMillis()-exit_time) > TimeConst.TIME_WAIT_FOR_EXIT_MS)
            {
                Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show();
                exit_time = System.currentTimeMillis();
            }
            else
            {
                stopAllSending();
                saveLastIPAndPort(); 
                data_trans.close();
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.MENU_ITEM_START_OR_REMOVE:
                startOrStopTransForExchange(current_list.getSelectedGroups());
                break;
            case R.id.MENU_ITEM_ALL_SELECT:
                if(has_input_ip)
                    current_list.setAllChecked(true);
                break;
            case R.id.MENU_ITEM_CANCEL_SELECT:
                if(has_input_ip)
                    current_list.setAllChecked(false);
                break;
            case R.id.MENU_ITEM_RUN_MODE:
                if(is_send_after_pause)
                {
                    is_send_after_pause =false;
                    option_menu.getItem(3).setTitle(getApplicationContext().getString(R.string.send_after_pause));
                }
                else
                {
                    is_send_after_pause =true;
                    option_menu.getItem(3).setTitle(getApplicationContext().getString(R.string.stop_after_pause));
                }
                break;
            case R.id.MENU_ITEM_ABOUT:
                showAboutInfo();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    private boolean afterInput()
    {
        String raw_str=et.getText().toString();
        String[] ip_and_port=testIPAndPORT(raw_str);
        if(ip_and_port==null)
        {
            Context c=getApplicationContext();
            Toast.makeText(c, c.getString(R.string.ip_error), Toast.LENGTH_SHORT).show();
            return false;
        }
        et.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        newIPAndPORT(ip_and_port[0],Integer.parseInt(ip_and_port[1]));
        if(last_ip.size()>=STORED_IPS)
            last_ip.remove(0);
        int l_s=last_ip.size();
        for(int i=0;i<l_s;i++)
            if(raw_str.equals(last_ip.get(i)))
                return true;
        last_ip.add(et.getText().toString());

        return true;
    }
    private String[] testIPAndPORT(String ip_str)
    {
        String[] ip_and_port=ip_str.split("@");
        if(ip_and_port.length!=2)
            return null;
        if(!ip_and_port[0].matches("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\." +
                "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."+
                "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."+
                 "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$"))
            return null;
        if(!ip_and_port[1].matches("^([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])$"))
            return null;
        return ip_and_port;
    }
    private void newIPAndPORT(String ip,int port)
    {
        tx.setText(getString(R.string.outline));
        tx.setTextColor(getResources().getColor(R.color.red));
        stopAllSending();
        data_trans.reset();
        data_trans.startTrans(ip,port);
    }
    private void exchangeSelectedGroup(GroupItem tmp)
    {
        tmp.setChildren(another_children);
        tmp.setCBoxVisibility(another_list.isCheckboxVisible);
        tmp.parent=another_list;
        another_list.insertGroup(0,tmp);
        current_list.removeGroup(tmp);
        if(current_list.groupCount()==0)
            current_list.isCheckboxVisible=false;
    }
    private void startOrStopTransForExchange(GroupItem item)
    {
        current_list.collapseAll();
        exchangeSelectedGroup(item);
        SensorOperation op=sensors.get(item.sensorType);
        if(item.parent==sending_list&&connected_server)
        {
            item.image = BitmapFactory.decodeResource(getResources(), R.drawable.sending);
            op.startTrans();
        }
        else
        {
            item.image = null;
            op.stopTrans();
        }
        sending_list.refresh();
        sensor_list.refresh();
    }
    private void startOrStopTransForExchange(ArrayList<GroupItem> items)
    {
        current_list.collapseAll();
        if(items==null) return;
        int s_size=items.size();
        for(int i=0;i<s_size;i++)
        {
            GroupItem item=items.get(i);
            exchangeSelectedGroup(item);
            SensorOperation op=sensors.get(item.sensorType);
            if(item.parent==sending_list&&connected_server)
            {
                item.image = BitmapFactory.decodeResource(getResources(), R.drawable.sending);
                op.startTrans();
            }
            else
            {
                item.image = null;
                op.stopTrans();
            }
        }
        sending_list.refresh();
        sensor_list.refresh();
    }
    private void startOrStopTransForSendingList(SensorOperation op,boolean start_or_stop)
    {
        if(!start_or_stop&&op.isTrans)
        {
            op.item.image = null;
            op.stopTrans();
            sending_list.refresh();
        }
        else if(start_or_stop&&!op.isTrans&&connected_server)
        {
            op.item.image = BitmapFactory.decodeResource(getResources(), R.drawable.sending);
            op.startTrans();
            sending_list.refresh();
        }
    }
    public void listChildClickHandle(GroupItem p, ChildItem c)
    {
        SensorOperation op=sensors.get(p.sensorType);
         switch (c.id)
         {
             case CHILD_ITEM_TO_START_SENDING:
             case CHILD_ITEM_REMOVE:
                 startOrStopTransForExchange(p);
                 break;
             case CHILD_ITEM_DETAIL_INFO:
                 showSensorInfo(op);
                 break;
             case CHILD_ITEM_START_SENDING:
                 startOrStopTransForSendingList(op,true);
                 break;
             case CHILD_ITEM_STOP_SENDING:
                 startOrStopTransForSendingList(op,false);
                 break;
             case CHILD_ITEM_FEQ:
                 item_of_context_menu=p;
                 openContextMenu(sending_list.list);
                 break;
             default:
                 break;
         }
    }
    private void stopAllSending()
    {
        int g_s=sending_list.groupCount();
        for(int i=0;i<g_s;i++)
        {
            SensorOperation op=sensors.get(sending_list.getGroup(i).sensorType);
            if(op.isTrans)
                startOrStopTransForSendingList(op,false);
        }
    }
    private HashMap<String,SensorOperation> getAvailableSensor()
    {
        List<Sensor> all_sensors=sm.getSensorList(Sensor.TYPE_ALL);
        HashMap<String,SensorOperation> sensor_table=new HashMap<String, SensorOperation>();
        sensor_list_groups=new ArrayList<GroupItem>();
        int s_size=all_sensors.size();
        String pack_head_str=null;
        GroupItem list_item=null;
        String unit=null;
        for(int i=0;i<s_size;i++)
        {
            switch (all_sensors.get(i).getType())
            {
                case Sensor.TYPE_ACCELEROMETER:
                    pack_head_str="ACCE";
                    unit="m/s^2";
                    list_item=new GroupItem(getString(R.string.accelerometer),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_AMBIENT_TEMPERATURE:
                    pack_head_str="TEMP";
                    unit="℃";
                    list_item=new GroupItem(getString(R.string.temperature),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_GRAVITY:
                    pack_head_str="GRAV";
                    unit="m/s^2";
                    list_item=new GroupItem(getString(R.string.gravity),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    pack_head_str="GYRO";
                    unit="rad/s";
                    list_item=new GroupItem(getString(R.string.gyroscope),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_LIGHT:
                    pack_head_str="LIGH";
                    unit="lux";
                    list_item=new GroupItem(getString(R.string.light),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    pack_head_str="LINA";
                    unit="m/s^2";
                    list_item=new GroupItem(getString(R.string.linear_acceleration),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    pack_head_str="MAGN";
                    unit="μT";
                    list_item=new GroupItem(getString(R.string.magnetic_field),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_ORIENTATION:
                    pack_head_str="ORIE";
                    unit="";
                    list_item=new GroupItem(getString(R.string.orientation),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_PRESSURE:
                    pack_head_str="PRES";
                    unit="hPa";
                    list_item=new GroupItem(getString(R.string.pressure),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_PROXIMITY:
                    pack_head_str="PROX";
                    unit="cm";
                    list_item=new GroupItem(getString(R.string.proximity),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_RELATIVE_HUMIDITY:
                    pack_head_str="HUMI";
                    unit="%";
                    list_item=new GroupItem(getString(R.string.humidity),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                case Sensor.TYPE_ROTATION_VECTOR:
                    pack_head_str="ROTV";
                    unit="";
                    list_item=new GroupItem(getString(R.string.rotation_vector),
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
                default:
                    pack_head_str="UNKO";
                    unit="";
                    list_item=new GroupItem("unkown",
                            all_sensors.get(i).getName(),pack_head_str,sensor_list_children);
                    break;
            }
            sensor_table.put(pack_head_str, new SensorOperation(all_sensors.get(i), pack_head_str, list_item,unit));
            sensor_list_groups.add(list_item);
        }
        return sensor_table;
    }
    private void showSensorInfo(SensorOperation op)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View info_view = layoutInflater.inflate(R.layout.detail_info, null);
        final Dialog dialog = new AlertDialog.Builder(this).
                setTitle(op.item.majorText).
                setIcon(R.drawable.sensor).
                setView(info_view).
                setPositiveButton(getString(R.string.ok), null).
                create();


        dialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            private Button positive_bn;
            @Override
            public void onShow(DialogInterface dialogInterface)
            {
                positive_bn = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                positive_bn.setTextSize(15);
            }
        });
        dialog.show();

        TextView tx_name=(TextView)dialog.findViewById(R.id.text_info_name);
        TextView tx_version=(TextView)dialog.findViewById(R.id.text_info_version);
        TextView tx_vendor=(TextView)dialog.findViewById(R.id.text_info_vendor);
        TextView tx_max_range=(TextView)dialog.findViewById(R.id.text_info_max_range);
        TextView tx_resol=(TextView)dialog.findViewById(R.id.text_info_resol);
        TextView tx_min_delay=(TextView)dialog.findViewById(R.id.text_info_min_delay);
        TextView tx_power=(TextView)dialog.findViewById(R.id.text_info_power);

        tx_name.setText(op.getName());
        tx_version.setText(op.getVersion());
        tx_vendor.setText(op.getVendor());
        tx_max_range.setText(op.getMaxRange());
        tx_resol.setText(op.getResolution());
        tx_min_delay.setText(op.getMinDelay());
        tx_power.setText(op.getPower());

    }
    private void showAboutInfo()
    {
        final Dialog dialog = new AlertDialog.Builder(this).
                setTitle(getString(R.string.about)).
                setIcon(R.drawable.sensor).
                setMessage(getString(R.string.about_step1)+"\n"+
                        getString(R.string.about_step2)+"\n"+
                        getString(R.string.about_step3)+"\n"+
                        getString(R.string.about_step4)+"\n"+
                        getString(R.string.about_step5)+"\n"+
                        "\n"+"   "+
                        getString(R.string.about_signature)+"\n").
                setPositiveButton(getString(R.string.ok), null).
                create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            private Button positive_bn;
            @Override
            public void onShow(DialogInterface dialogInterface)
            {
                positive_bn = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                positive_bn.setTextSize(15);
            }
        });
        dialog.show();
    }
    void getLastIPAndPort()
    {
        last_ip=new ArrayList<String>();
        String fname=getFilesDir().getAbsolutePath()+"/"+IP_FILE_NAME;
        File file=new File(fname);
        if(!file.exists())
        {
            try {file.createNewFile();return;}
            catch (IOException e) {}
        }
        try
        {
            FileInputStream ip_file=new FileInputStream(file);
            int length = ip_file.available();
            byte [] buffer = new byte[length];
            ip_file.read(buffer);
            String all_str=new String(buffer,"UTF-8");
            String[] ip_and_port=all_str.split("\n");
            for(int i=0;i<ip_and_port.length;i++)
                last_ip.add(ip_and_port[i]);
        }
        catch (FileNotFoundException e) {}
        catch (IOException e) {}
    }
    void saveLastIPAndPort()
    {
        String fname=getFilesDir().getAbsolutePath()+"/"+IP_FILE_NAME;
        File file=new File(fname);
        if(!file.exists())
            return;
        try
        {
            FileOutputStream ip_file=new FileOutputStream(file);
            String all_str=new String();
            int l_s=last_ip.size();
            for(int i=0;i<l_s;i++)
                all_str+=last_ip.get(i)+"\n";
            ip_file.write(all_str.getBytes());
        }
        catch (FileNotFoundException e) {}
        catch (IOException e) {}
    }
    class MsgHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg)
        {

            switch (msg.what)
            {
                case EventType.TRANS_START:
                    tx.setText(getString(R.string.inline));
                    tx.setTextColor(getResources().getColor(R.color.green));
                    connected_server=true;
                    data_trans.restartTrans(0);
                    break;
                case EventType.NEW_MSG:
                    SensorOperation op=sensors.get((String)msg.obj);
                    if(op!=null&&op.isTrans)
                        startOrStopTransForSendingList(op,false);
                    break;
                case EventType.HEARTBEAT_CLOSE:
                    tx.setText(getString(R.string.outline));
                    tx.setTextColor(getResources().getColor(R.color.red));
                    connected_server=false;
                    data_trans.reset();
                    data_trans.restartTrans(TimeConst.TRY_RECON_MS);
                    stopAllSending();
                    break;
                case EventType.CONNECT_FAIL:
                    data_trans.reset();
                    data_trans.restartTrans(TimeConst.TRY_RECON_MS);
                    break;
                default:
                    break;
            }
        }
    }
    class IPInputListener implements View.OnKeyListener
    {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent)
        {
            if ( i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
            {
                if(afterInput()&&!has_input_ip)
                {
                    has_input_ip=true;
                    sensor_list.collapseAll();
                    sensor_list_children.clear();
                    sensor_list_children.add(new ChildItem(getString(R.string.to_sending_start),CHILD_ITEM_TO_START_SENDING));
                    sensor_list_children.add(new ChildItem(getString(R.string.detail_info),CHILD_ITEM_DETAIL_INFO));
                    int s_g=sensor_list_groups.size();
                    for(int j=0;j<s_g;j++)
                        sensor_list_groups.get(j).setChildren(sensor_list_children);
                    sensor_list.enableLongClick(true);
                    sending_list.enableLongClick(true);
                    sensor_list.refresh();
                }
                return true;
            }
            return false;
        }
    }
    class HasFocus implements View.OnFocusChangeListener
    {
        @Override
        public void onFocusChange(View view, boolean hasFocus)
        {
            if(hasFocus)
            {
                SensorViewerAMainActivity a=SensorViewerAMainActivity.this;
                String[] ip_str=new String[last_ip.size()];
                last_ip.toArray(ip_str);
                ArrayAdapter adapter = new ArrayAdapter(a,android.R.layout.simple_list_item_1, ip_str);
                et.setAdapter(adapter);
            }
        }
    }
}











